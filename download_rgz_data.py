import requests
from bs4 import BeautifulSoup
import zipfile
import os
import shutil
import time
from common import retry_on_error


@retry_on_error(timeout_in_seconds=5*60)
def download_file(url, zip_filename, target_dir, expected_file):
    print(f'Downloading {zip_filename}')
    r = s.get(url, stream=True)
    print(f'Status code {r.status_code}')
    if r.status_code >= 500:
        raise ConnectionRefusedError('server error')
    with open(zip_filename, 'wb') as fd:
        for chunk in r.iter_content(chunk_size=1023):
            fd.write(chunk)
    with zipfile.ZipFile(zip_filename, 'r') as zip_ref:
        source = zip_ref.open(target_dir)
        target = open(expected_file, "wb")
        with source, target:
            shutil.copyfileobj(source, target)

    os.remove(zip_filename)

    if not os.path.exists(expected_file):
        print(f'File {expected_file} still do not exist')
        raise ConnectionRefusedError


if not os.path.exists('rgz-password'):
    print('Please create file rgz-password with RGZ credentials in the form of <username>:<password> in it')
    exit()

with open('rgz-password') as f:
    line = f.readline()
username = line.split(":")[0].strip()
password = line.split(":")[1].strip()

s = requests.Session()
# Go to login page
r = s.get('https://opendata.geosrbija.rs/loginopendata')
soup = BeautifulSoup(r.text, 'lxml')
csrf_token = soup.select_one('#csrf_token')['value']

# Login
headers = {}
headers['content-type'] = 'application/x-www-form-urlencoded'
r = s.post('https://opendata.geosrbija.rs/loginopendata', headers=headers, data={'username': username, 'password': password, 'csrf_token': csrf_token})

# Get okrug
download_file('https://opendata.geosrbija.rs/okrug?f=csv&user={0}'.format(username), 'input/okrug.zip', 'okrug.csv', 'input/okrug.csv')
time.sleep(5)

# Get grad
download_file('https://opendata.geosrbija.rs/gradovi?f=csv&user={0}'.format(username), 'input/grad.zip', 'gradovi.csv', 'input/gradovi.csv')
time.sleep(5)

# Get opstina
download_file('https://opendata.geosrbija.rs/opstina?f=csv&user={0}'.format(username), 'input/opstina.zip', 'opstina.csv', 'input/opstina.csv')
time.sleep(5)

# Get naselje
download_file('https://opendata.geosrbija.rs/naselje?f=csv&user={0}'.format(username), 'input/naselje.zip', 'tmp/data/ready/naselja/naselje.csv', './input/naselje.csv')
time.sleep(5)

# Get mesne zajednice
download_file('https://opendata.geosrbija.rs/mesna_zajednica?f=csv&user={0}'.format(username), 'input/mesna_zajednica.zip', 'tmp/data/ready/mz/mesna_zajednica.csv', './input/mesna_zajednica.csv')
