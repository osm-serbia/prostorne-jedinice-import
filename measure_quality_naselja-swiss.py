import csv
import os
import sys
import time

import fiona
import overpy
from shapely.geometry import shape
import pyproj
from common import retry_on_error, create_geometry_from_osm_response, get_polygon_by_mb
from common import get_municipality_district, get_settlement_municipality, get_district_name_by_id
from concurrent.futures import ThreadPoolExecutor, ProcessPoolExecutor, as_completed
from threading import Lock
import multiprocessing
import shapely

csv.field_size_limit(sys.maxsize)
geod = pyproj.Geod(ellps='WGS84')
progress_file = 'swiss/swiss-measurements.csv'
csv_write_mutex = Lock()
results = []


def get_current_results():
    # Load saved state
    results = []
    if os.path.isfile(progress_file):
        with open(progress_file) as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                results.append(
                    {'kanton_nummer': row['kanton_nummer'], 'bezirk_nummer': row['bezirk_nummer'],  'bfs_nummer': row['bfs_nummer'], 'settlement_name': row['settlement_name'],
                     'osm_settlement': row['osm_settlement'], 'relation_id': row['relation_id'],
                     'area_diff': row['area_diff'], 'area_not_shared': row['area_not_shared'],
                     'national_border': row['national_border']})
    return results


def get_naselja_from_shapefile():
    naselja = []
    with fiona.open("swiss/swiss4326.shp", "r", encoding='ISO-8859-1') as input:
        for i in input:
            settlement_polygon = shape(i['geometry'])
            kanton_nummer = i['properties']['KANTONSNUM']
            bezirk_nummer = i['properties']['BEZIRKSNUM']
            settlement_name = i['properties']['NAME']
            bfs_nummer = int(i['properties']['BFS_NUMMER'])
            naselja.append({'settlement_polygon': shapely.force_2d(settlement_polygon),
                            'kanton_nummer': kanton_nummer, 'bezirk_nummer': bezirk_nummer,
                            'bfs_nummer': bfs_nummer, 'settlement_name': settlement_name})
    return naselja


def write_results(current_results):
    with csv_write_mutex:
        with open(progress_file, 'w') as out_csv:
            writer = csv.DictWriter(out_csv, fieldnames=[
                'kanton_nummer', 'bezirk_nummer', 'bfs_nummer', 'settlement_name',
                'osm_settlement', 'relation_id', 'area_diff', 'area_not_shared', 'national_border'])
            writer.writeheader()
            for data in current_results:
                writer.writerow(data)


def process_settlement(overpass_api, settlement, count_processed, total_to_process):
    global results
    print('Processed {0}/{1}'.format(count_processed, total_to_process))

    settlement_polygon = settlement['settlement_polygon'].buffer(0)
    kanton_nummer = settlement['kanton_nummer']
    bezirk_nummer = settlement['bezirk_nummer']
    bfs_nummer = settlement['bfs_nummer']
    settlement_name = settlement['settlement_name']

    print('Processing settlement {0} (bezirk {1})'.format(settlement_name, bezirk_nummer))

    overpass_settlement_polygon, osm_settlement_name, osm_relation_id, national_border = get_polygon_by_mb(overpass_api, 8, bfs_nummer, country='Schweiz/Suisse/Svizzera/Svizra', mb_key='swisstopo:BFS_NUMMER')
    if overpass_settlement_polygon is None:
        print('Settlement {0} (bezirk {1}) not found using swisstopo:BFS_NUMMER'.format(settlement_name, bezirk_nummer))
        result = {'kanton_nummer': kanton_nummer, 'bezirk_nummer': bezirk_nummer, 'settlement_name': settlement_name, 'bfs_nummer': bfs_nummer,
                  'osm_settlement': '', 'relation_id': -1, 'area_diff': -1, 'area_not_shared': -1,
                  'national_border': national_border}
        results.append(result)
        return result

    settlement_area = settlement_polygon.area
    intersection_area = settlement_polygon.intersection(overpass_settlement_polygon).area

    a_minus_b = settlement_polygon.difference(overpass_settlement_polygon)
    b_minus_a = overpass_settlement_polygon.difference(settlement_polygon.buffer(0))
    a_minus_b_union_b_minus_a = a_minus_b.union(b_minus_a)
    a_union_b = settlement_polygon.union(overpass_settlement_polygon)
    area_not_shared = a_minus_b_union_b_minus_a.area / a_union_b.area

    print(bezirk_nummer, settlement_name, osm_settlement_name, 100 * intersection_area/settlement_area, area_not_shared)
    result = {'kanton_nummer': kanton_nummer, 'bezirk_nummer': bezirk_nummer, 'bfs_nummer': bfs_nummer, 'settlement_name': settlement_name,
              'osm_settlement': osm_settlement_name, 'relation_id': osm_relation_id,
              'area_diff': round(intersection_area/settlement_area, 5),
              'area_not_shared': round(area_not_shared, 5),
              'national_border': national_border}
    results.append(result)
    return result


def measure_quality_settlement(overpass_api):
    global results
    results = get_current_results()
    settlements = get_naselja_from_shapefile()

    count_processed = 1
    all_futures = []
    thread_count = 1 if 'localhost' not in overpass_api.url else multiprocessing.cpu_count()
    print('Using {0} threads'.format(thread_count))
    # with ProcessPoolExecutor(max_workers=thread_count) as executor:
    #     for settlement in settlements:
    #         # Skip if already processed
    #         bezirk_nummer = settlement['bezirk_nummer']
    #         settlement_name = settlement['settlement_name']
    #         if any((r for r in results if r['bezirk_nummer'] == bezirk_nummer and r['settlement_name'] == settlement_name)):
    #             print('Bezirk {0} and settlement {1} already processed'.format(bezirk_nummer, settlement_name))
    #             continue
    #
    #         future = executor.submit(process_settlement, overpass_api, settlement, count_processed, len(settlements))
    #         all_futures.append(future)
    #         count_processed = count_processed + 1
    #     for future in as_completed(all_futures):
    #         results.append(future.result())

    for settlement in settlements:
        # Skip if already processed
        bezirk_nummer = settlement['bezirk_nummer']
        if bezirk_nummer:
            bezirk_nummer = str(bezirk_nummer)
        else:
            bezirk_nummer = ''
        settlement_name = settlement['settlement_name']
        if any((r for r in results if r['bezirk_nummer'] == bezirk_nummer and r['settlement_name'] == settlement_name)):
            print('Bezirk {0} and settlement {1} already processed'.format(bezirk_nummer, settlement_name))
            continue

        process_settlement(overpass_api, settlement, count_processed, len(settlements))
        count_processed = count_processed + 1
        write_results(results)
        time.sleep(5)
    write_results(results)


if __name__ == '__main__':
    #overpass_api = overpy.Overpass(url='https://lz4.overpass-api.de/api/interpreter')
    overpass_api = overpy.Overpass(url='http://overpass-api.de/api/interpreter')
    #overpass_api = overpy.Overpass(url='http://localhost:12345/api/interpreter')
    measure_quality_settlement(overpass_api)
